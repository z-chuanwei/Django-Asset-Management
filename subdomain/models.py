from django.db import models

# Create your models here.

class DomainList(models.Model):
    domain_name = models.TextField(max_length=128, null=True, blank=True)
    domain_time = models.TextField(max_length=128, null=True, blank=True)
    domain_remarks = models.TextField(max_length=128, null=True, blank=True)
    domain_finished = models.BooleanField(default=False)


class SubdomainDetail(models.Model):
    url = models.TextField(max_length=128, null=True, blank=True)
    port = models.TextField(max_length=128, null=True, blank=True)
    cname = models.TextField(max_length=128, null=True, blank=True)
    ip = models.TextField(max_length=128, null=True, blank=True)
    status = models.TextField(max_length=128, null=True, blank=True)
    title = models.TextField(max_length=128, null=True, blank=True)
