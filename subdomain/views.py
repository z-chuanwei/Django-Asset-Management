from django.shortcuts import render
from plugins.subdomain import oneforall_start, oneforall_result
from datetime import datetime
from subdomain import models
# Create your views here.


def index(request):
    if request.method == 'POST':
        domain = request.POST.get('domain')
        print(domain)
        # 存入收集列表数据库
        remark = request.POST.get('remark')
        domain_time = datetime.now()
        models.DomainList.objects.create(domain_name=domain, domain_time=domain_time, domain_remarks=remark, domain_finished=False)
        # 开始oneforall子域名收集模块
        oneforall_start(domain)
    # GET请求则展示列表
    domainlist = models.DomainList.objects.all()
    return render(request, 'subdomain/index.html', {'domainlist': domainlist})

def detail(request):
    domain = request.GET.get('domainname')
    print(domain)
    res = oneforall_result(domain)
    print(res[0])
    return render(request, 'subdomain/detail.html', {'context': res})
