from django.shortcuts import render
from plugins.waf_check import waf_check
from plugins.cdn_check import cdn_check
from plugins.whatweb import whatweb
# Create your views here.

def index(request):
    domain = "https://www.yunaq.com"
    # 1.cdn识别
    # ip_list = cdn_check(domain)
    # print(len(ip_list))
    # if len(ip_list) == 1:
    #     print("real ip= " + ip_list[0])
    # else:
    #     print("found cdn!!!")

    # 2. waf识别
    waf_check(domain)



    # Web指纹
    # result = whatweb(domain)
    # print(result)
    # {'web-servers': ['LiteSpeed'], 'font-scripts': ['Google Font API'], 'tag-managers': ['Google Tag Manager'], 'video-players': ['YouTube'], 'cms': ['Joomla'], 'programming-languages': ['PHP']}



    return render(request, 'scan/index.html')

def detail(request):
    print(request.GET.get('urlname'))
    return render(request, 'scan/detail.html')

