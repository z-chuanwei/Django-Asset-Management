from django.db import models

# Create your models here.

# 扫描资产
class ScanAssets(models.Model):
    assets_name = models.TextField(max_length=128, null=True, blank=True, verbose_name='资产名称')
    assets_time = models.TextField(max_length=128, null=True, blank=True, verbose_name='创建时间')
    assets_remarks = models.TextField(max_length=128, null=True, blank=True, verbose_name='资产备注')
    assets_finished = models.BooleanField(default=False, verbose_name='完成状态')

# Web资产
class WebInfo(models.Model):
    web_cms = models.TextField(max_length=128, null=True, blank=True, verbose_name='CMS')
    web_waf = models.TextField(max_length=128, null=True, blank=True, verbose_name='WAF')
    web_cdn = models.TextField(max_length=128, null=True, blank=True, verbose_name='CDN')
    web_server = models.TextField(max_length=128, null=True, blank=True, verbose_name='Web服务器')
    web_language = models.TextField(max_length=128, null=True, blank=True, verbose_name='Web开发语言')
    web_system = models.TextField(max_length=128, null=True, blank=True, verbose_name='操作系统')
