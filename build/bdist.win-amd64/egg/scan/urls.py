#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 2021/4/21 14:41
# @Author   : zenggt

from django.urls import path
from . import views


urlpatterns = [
    path('index.html', views.index),
    path('detail.html', views.detail),
]