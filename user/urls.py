#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 2021/4/22 9:54
# @Author   : zenggt

from django.urls import path
from . import views
urlpatterns = [
    path('register', views.register),
    path('sendsms', views.sendsms, name='sendsms'),
]