from django.core.validators import RegexValidator
from django.core.validators import ValidationError
from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from user import models
from django import forms
from django.conf import settings
from plugins.tencent.sms import send_sms_single
import random
from django_redis import get_redis_connection



# Create your views here.


class RegisterModelForm(forms.ModelForm):
    # 定义手机号格式要求
    mobile_phone = forms.CharField(label='手机号', validators=[RegexValidator(r'^(1[3|4|5|6|7|8|9])\d{9}$', '手机号格式错误'),])
    # 定义密码不可见
    password = forms.CharField(label='密码', widget=forms.PasswordInput())
    confirm_password = forms.CharField(label='重复密码', widget=forms.PasswordInput())
    # 手机验证码
    mobile_code = forms.CharField(label='验证码')

    class Meta:
        model = models.UserInfo
        fields = "__all__"
    # 重写init,完成标签属性
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name,field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            field.widget.attrs['placeholder'] = '请输入%s' % (field.label)


def register(request):
    form = RegisterModelForm()
    return render(request, 'user/register.html', {'form': form})


class SendSmsForm(forms.Form):
    mobile_phone = forms.CharField(label='手机号', validators=[RegexValidator(r'^(1[3|4|5|6|7|8|9])\d{9}$', '手机号格式错误'),])

    # 自定义init:接收request
    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request

    # 手机号验证勾子
    def clean_mobile_phone(self):
        mobile_phone = self.cleaned_data['mobile_phone']

        # 短信模板验证
        tpl = self.request.GET.get('tpl')
        templates_id = settings.TENCENT_SMS_TEMPLATES[tpl]
        if not templates_id:
            raise ValidationError('短信模板错误')

        # 校验手机号是否存在
        exists = models.UserInfo.objects.filter(mobile_phone=mobile_phone).exists()
        if exists:
            raise ValidationError('手机号已存在')

        # 发短信
        code = random.randrange(1000,9999)
        sms = send_sms_single(mobile_phone, templates_id, [code, ])
        if sms['result'] != 0:
            raise ValidationError('短信发送失败, {}'.format(sms['errsmg']))

        # 将验证码写入redis(django-redis)
        conn = get_redis_connection() # 默认default
        conn.set(mobile_phone, code, ex=60) # 超时60s

        return mobile_phone

def sendsms(request):
    form = SendSmsForm(request, data=request.GET)
    # 是否为空、格式是否正确
    if form.is_valid():
        return JsonResponse({'status': True})
    return JsonResponse({'status': False, 'error': form.errors })