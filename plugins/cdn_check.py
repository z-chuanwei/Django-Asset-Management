#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 2021/4/21 19:25
# @Author   : zenggt
import socket
from rich.console import Console

console = Console()

# 判断CDN函数
def cdn_check(domain):
    ip_list = []
    try:
        console.print('正在进行CDN检测', style="#ADFF2F")
        addrs = socket.getaddrinfo(domain, None, family=0)
        print(addrs)
        for item in addrs:
            if item[4][0] not in ip_list:
                # 如果只返回一个ip,则判读那无cdn
                if item[4][0].count('.') == 3:
                    ip_list.append(item[4][0])
                else:
                    pass
        return ip_list
    except:
        console.print('CDN检测失败，请检查输入格式', style="bold red")
        pass
