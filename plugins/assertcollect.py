#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 2021/4/23 12:49
# @Author   : zenggt
import socket
from rich.console import Console
from plugins import config
import subprocess
import builtwith
from urllib.parse import urlparse
import nmap

console = Console()


# 1. cdn识别
# 输入：www.365cyd.com（netloc部分）
# 输出：len(iplist) == 1  --》无cdn
def cdn_check(domain):
    domain_netloc = urlparse(domain).netloc
    ip_list = []
    try:
        console.print('正在进行CDN检测', style="#ADFF2F")
        addrs = socket.getaddrinfo(domain_netloc, None, family=0)
        # print(addrs)
        for item in addrs:
            if item[4][0] not in ip_list:
                # 排除掉getaddrinfo返回的ipv6地址
                if item[4][0].count('.') == 3:
                    print(item[4][0])
                    ip_list.append(item[4][0])
                else:
                    pass
        console.print('CDN检测完成', style="#ADFF2F")
        return ip_list
    except:
        console.print('CDN检测失败，请检查输入格式', style="bold red")
        pass

# 2.waf识别
# 输入：https://www.365cyd.com/、www.365cyd.com
# 输出：返回识别信息
def waf_check(domain):
    res = ' '
    try:
        console.print('正在进行WAF检测', style="#ADFF2F")
        cmd = 'python ' + config.wafw00f_path + ' ' + domain
        # print(cmd)
        rsp = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        result = rsp.stdout.read().decode("GBK").split('\n')
        for i in result:
            if 'site' in i:
                res = i
        while True:
            if rsp.poll() == None:
                pass
            else:
                break
        console.print('WAF检测完成', style="#ADFF2F")
        return res
    except:
        console.print('WAF检测失败，请检查输入格式', style="bold red")
        pass


# 3.Web指纹识别
# 输入：https://wordpress.org
# 输出：指纹信息列表
# {'web-servers': ['LiteSpeed'], 'font-scripts': ['Google Font API'], 'tag-managers': ['Google Tag Manager'],
# 'video-players': ['YouTube'], 'cms': ['Joomla'], 'programming-languages': ['PHP']}
def whatweb(domain):
    try:
        console.print('正在进行Web指纹检测', style="#ADFF2F")
        tech_used = builtwith.parse(domain)
        console.print('Web指纹检测完成', style="#ADFF2F")
        return tech_used
    except:
        console.print('Web指纹检测失败，请检查输入格式', style="bold red")
        pass

# 4.端口信息
def portscan(host, port):
    port_list = []
    # 实例化PortScanner
    scan = nmap.PortScanner()
    # 调用scan方法
    scan.scan(host, port, '-Pn')
    # 打印命令
    print(scan.command_line())
    # 获取扫描结果
    print(scan.scaninfo())
    # 获取扫描端口
    lport = scan[host]['tcp'].keys()
    for p in lport:
        if scan[host]['tcp'][p]['state'] == 'open':
            port_list.append(p)
    return port_list



