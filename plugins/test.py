#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 2021/4/22 11:28
# @Author   : zenggt
import masscan

mas = masscan.PortScanner()
mas.scan('http://192.168.18.1', ports='22,80,8080', arguments='--max-rate 1000')
print(mas.scan_result)