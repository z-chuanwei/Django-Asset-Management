#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 2021/4/21 21:48
# @Author   : zenggt

# wafw00f
wafw00f_path = '.\\plugins\\wafw00f\\wafw00f\\main.py'
# portscan
portscan_path = '.\\plugins\\portscan.py'
# xray
xray_path = '.\\tools\\xray\\xray_windows.exe'
# crawlergo
crawlergo_path = '.\\tools\\crawlergo\\crawlergo.exe'
# chrome
chrome_path = '.\\tools\\chrome\\chrome.exe'
# oneforall
oneforall_path = '.\\tools\\oneforall\\oneforall.py'
oneforall_sql_path = '.\\tools\\oneforall\\results\\result.sqlite3'