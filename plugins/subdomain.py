#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 2021/4/18 15:14
# @Author   : zenggt

import subprocess
from plugins import config
import sqlite3

def oneforall_start(target):
    cmd = 'python ' + config.oneforall_path + ' --target ' + target + ' run'
    print(cmd)
    try:
        proc = subprocess.Popen(cmd, shell=True)
        print('oneforall启动成功，正在收集子域名...')
    except:
        print('oneforall启动失败，请检查输入格式')


def oneforall_result(domain):
    # 连接oneforall数据库
    conn = sqlite3.connect(config.oneforall_sql_path)
    # 创建游标对象
    cur = conn.cursor()
    # 查询
    table_name = domain.replace('.', '_')
    print(table_name)
    sql_cmd = 'SELECT url, port, cname, ip, status, title FROM ' + table_name
    res = cur.execute(sql_cmd)
    return res.fetchall()
