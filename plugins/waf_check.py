#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 2021/4/21 21:49
# @Author   : zenggt
from plugins import config
import subprocess
from rich.console import Console

console = Console()

def waf_check(domain):
    res = []
    console.print('正在进行WAF检测', style="#ADFF2F")
    cmd = 'python ' +  config.wafw00f_path + ' ' + domain
    print(cmd)
    rsp = subprocess.Popen(cmd, stdout=subprocess.PIPE,  stderr=subprocess.PIPE, shell=True)
    result = rsp.stdout.read().decode("GBK").split('\n')
    for i in result:
        if 'site' in i:
            res.append(i)
    while True:
        if rsp.poll() == None:
            pass
        else:
            break
    console.print('WAF检测完成', style="#ADFF2F")
    return res
