#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 2021/4/21 18:51
# @Author   : zenggt

import builtwith
from rich.console import Console

console = Console()


def whatweb(domain):
    console.print('正在进行Web指纹检测', style="#ADFF2F")
    tech_used = builtwith.parse(domain)
    return tech_used


