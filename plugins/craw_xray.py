from plugins import config
import subprocess
from threading import Timer


class Xray():
    def __init__(self):
        self.timeout = 300

    # 启动xray被动代理
    def xray_start(self, scan_name):
        # 提取scan_name
        sub0 = scan_name.split('.')[0]
        sub1 = sub0.split('//')[1]
        domain = scan_name.split('.')[1]
        # result_name = '/templates/scan_result/' + sub1 + domain + '.html'
        result_name = sub1 + domain + '.html'
        # print(result_name)
        cmd = config.xray_path + " webscan --listen 127.0.0.1:7777 --webhook-output http://127.0.0.1:8000/scan/webhook --html-output " + result_name
        rsp = subprocess.Popen(cmd, shell=True)
        print(cmd)
        print('xray被动扫描已进入监听状态')
        # 定时器
        my_timer = Timer(self.timeout, self.timeout_cb, [rsp])
        my_timer.start()
    def timeout_cb(self, rsp):
        print('timeout exit!!!!')
        print(rsp.pid)
        rsp.terminate( )


# 启动crawlego动态爬虫并把结果推送给xray被动监听地址
def crawlego_start(craw_name):
    cmd = config.crawlergo_path + "  -c " + config.chrome_path + " -t 20 -f smart --fuzz-path --push-to-proxy " \
                                                                 "http://127.0.0.1:7777 --push-pool-max 20 " + craw_name
    rsp = subprocess.Popen(cmd, shell=True)
    print(cmd)
    print('craw爬虫已开始爬取目标')

