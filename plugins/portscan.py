#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 2021/4/22 10:01
# @Author   : zenggt
import nmap
import sys
import re
import optparse
import threading


class MyThread(threading.Thread):

    def __init__(self, target, args=()):
        super(MyThread, self).__init__()
        self.target = target
        self.args = args

    def run(self):
        self.result = self.target(*self.args)

    def get_result(self):
        try:
            # 如果子线程不使用join方法，此处可能会报没有self.result的错误
            return self.result
        except Exception:
            return None


def portscan(host, port):
    global port_list
    # 实例化PortScanner类
    scanner = nmap.PortScanner()
    # 调用scan()方法
    result = scanner.scan(host, port, '-Pn')
    # 获取扫描状态
    print(result)
    state = result['scan'][host]['tcp'][int(port)]['state']
    print("HOST:" + host + " tcp/" + port + " " + state)
    # 返回open端口
    if state == 'open':
        port_list = port
        print(port_list)


# 对port参数为 - 时进行解析,返回port list
def anlyze_port(target_port):
    try:
        pattern = re.compile(r'(\d+)-(\d+)')  # 解析连接符-模式
        match = pattern.match(target_port)
        if match:
            start_port = int(match.group(1))
            end_port = int(match.group(2))
            return ([x for x in range(start_port, end_port + 1)])
        else:
            return ([int(x) for x in target_port.split(',')])

    except Exception as err:
        print('请注意错误:', sys.exc_info()[0], err)
        exit(0)


def main(host, port_p):
    port_list = []
    target_host = host
    target_port = port_p
    target_port = anlyze_port(target_port)

    for port in target_port:
        t = MyThread(target=portscan, args=(target_host, str(port)))
        t.start()

    return port_list

if __name__ == '__main__':
    main('202.115.196.5', '80,443')

