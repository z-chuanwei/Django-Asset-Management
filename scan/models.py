from django.db import models

# Create your models here.

# 扫描资产
class ScanAssets(models.Model):
    assets_name = models.TextField(max_length=128, null=True, blank=True, verbose_name='资产名称')
    assets_time = models.TextField(max_length=128, null=True, blank=True, verbose_name='创建时间')
    assets_remarks = models.TextField(max_length=128, null=True, blank=True, verbose_name='资产备注')
    assets_finished = models.BooleanField(default=False, verbose_name='完成状态')

# Web资产
class WebInfo(models.Model):
    domain_name = models.TextField(max_length=128, null=True, blank=True, default=None, verbose_name='资产名称')
    web_waf = models.TextField(max_length=128, null=True, blank=True,  default=None, verbose_name='WAF')
    web_cdn = models.TextField(max_length=128, null=True, blank=True,  default=None, verbose_name='CDN')
    web_info = models.TextField(max_length=128, null=True, blank=True,  default=None, verbose_name='Web指纹')

# 端口信息
class PortInfo(models.Model):
    domain_name = models.TextField(max_length=128, null=True, blank=True, verbose_name='资产名称')
    open_port = models.IntegerField(default=None, verbose_name='开放端口')


# 漏洞信息
class Scanbug(models.Model):
    url = models.TextField(max_length=128, null=True, blank=True)
    plugin = models.TextField(max_length=128, null=True, blank=True)
    scan_time = models.TextField(max_length=128, null=True, blank=True)
    host = models.TextField(max_length=128, null=True, blank=True)
    payload = models.TextField(max_length=128, null=True, blank=True)
    payload_request = models.TextField(max_length=128, null=True, blank=True)
    payload_response = models.TextField(max_length=128, null=True, blank=True)