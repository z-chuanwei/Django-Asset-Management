import json
import re

from django.shortcuts import render, redirect, HttpResponse
from django.http import JsonResponse
from plugins import assertcollect
from scan import models
from datetime import datetime
from plugins.craw_xray import Xray, crawlego_start
from urllib.parse import urlparse


# Create your views here.

count = 0


# 展示资产列表
def index(request):
    all_asserts = models.ScanAssets.objects.all().order_by('-id')
    return render(request, 'scan/index.html', {'assert': all_asserts})


# 添加资产
def addassert(request):
    domain_name = request.POST.get('domainName')
    print(request.POST)
    exist = models.ScanAssets.objects.filter(assets_name=domain_name).exists()
    # 资产已存在
    if exist:
        response = JsonResponse({"info": "exist"})
        return response
    else:
        # 添加资产
        remark_name = request.POST.get('remarkName')
        time_add = datetime.now()
        models.ScanAssets.objects.create(assets_name=domain_name, assets_remarks=remark_name, assets_time=time_add)
        response = JsonResponse({"info": "ok", "domain_name": domain_name})
        return response


# 收集资产
def assertstart(request):
    domain_name = request.POST.get('domain_name')

    # 1.先检测cdn是否存在
    ip_list = assertcollect.cdn_check(domain_name)
    print(ip_list)
    if len(ip_list) == 1:
        res_cdn = 'The site ' + domain_name + ' CDN NOT FOUND!'
        cdn_status = 'CDN NOT FOUND'
    else:
        res_cdn = 'The site ' + domain_name + ' CDN FOUND!!!'
        cdn_status = 'CDN FOUND'
    print(res_cdn)

    # 2.检测是否有waf
    res_waf = assertcollect.waf_check(domain_name)
    if 'security ' in res_waf:
        waf_status = 'NO WAF'
    elif len(res_waf):
        waf_status = 'NO WAF'
    else:
        waf_status = res_waf
    print(res_waf)

    # 3.检测Web指纹
    res_webcms = assertcollect.whatweb(domain_name)
    print(res_webcms)

    # 4.无cdn-->端口收集
    open_list = []
    if len(ip_list) == 1:
        host = ip_list[0]
        port = '80,1433,445,135,5985,3389,22,1521,3306,6379,5432,389,25,110,143,443,5900,21,873,27017,23,3690,1099,5984,5632'
        open_list = assertcollect.portscan(host, port)
        print(type(open_list))
        print(open_list)

    # 5.无waf-->漏洞扫描
    if waf_status == 'NO WAF':
        # 调用crawlego和xray接口进行漏洞扫描
        xray = Xray()
        xray.xray_start(domain_name)
        crawlego_start(domain_name)

    # Web资产信息
    models.WebInfo.objects.create(domain_name=domain_name, web_waf=waf_status, web_cdn=cdn_status, web_info=res_webcms)
    # 端口信息
    for i in range(len(open_list)):
        models.PortInfo.objects.create(domain_name=domain_name, open_port=open_list[i])

    return HttpResponse('start')


# webhook接口
def xray_webhook(request):
    global count
    # 获取webhook数据
    vul_data = json.loads(request.body)
    if 'detail' in str(request.body):
        content = """## xray 发现了新漏洞

        url: {url}

        插件: {plugin}

        payload: {payload}

        发现时间: {create_time}

        请及时查看和处理
        """.format(url=vul_data["data"]["target"]["url"], plugin=vul_data["data"]["plugin"],
                   payload=vul_data["data"]["detail"]["payload"],
                   create_time=str(datetime.fromtimestamp(vul_data["data"]["create_time"] / 1000)))
        print(content)
        # 漏洞入库
        url = vul_data["data"]["target"]["url"]
        plugin_name = vul_data["data"]["plugin"]
        create_time = str(datetime.fromtimestamp(vul_data["data"]["create_time"] / 1000))
        payload = vul_data["data"]["detail"]["payload"]
        payload_request = vul_data["data"]["detail"]["snapshot"][0][0]
        payload_response = vul_data["data"]["detail"]["snapshot"][0][1]
        host_re = re.findall('https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+', url)
        host = host_re[0]
        models.Scanbug.objects.create(url=url, plugin=plugin_name, scan_time=create_time, host=host, payload=payload,
                                      payload_request=payload_request, payload_response=payload_response)
        if count > 10:
            count = 0
            a = models.ScanAssets.objects.get(assets_name=host)
            a.assets_finished = True
            a.save()
            return redirect('/scan/index.html')
    else:
        count += 1
        print(count)
        print(vul_data)
    return redirect('/scan/index.html')


# 资产详情
def detail(request):
    urlname = request.GET.get('urlname')
    web_info = models.WebInfo.objects.get(domain_name=urlname)
    web_port = models.PortInfo.objects.filter(domain_name=urlname)
    bugdetail = models.Scanbug.objects.filter(host=urlname)
    print(bugdetail)
    # echars数据
    sql_count = models.Scanbug.objects.filter(plugin__contains='sqldet').count()
    xss_count = models.Scanbug.objects.filter(plugin__contains='xss').count()
    rce_count = models.Scanbug.objects.filter(plugin__contains='rce').count()
    dir_count = models.Scanbug.objects.filter(plugin__contains='dirscan').count()
    brute_count = models.Scanbug.objects.filter(plugin__contains='brute').count()
    return render(request, 'scan/detail.html', {'webinfo': web_info, 'webport': web_port,'bugs': bugdetail, 'sql': sql_count, 'xss': xss_count, 'rce': rce_count, 'dir': dir_count,
                   'brute': brute_count})


# 漏扫详情
def bugdetail(request):
    url = request.GET.get('urlname')
    print(url)
    detail = models.Scanbug.objects.filter(url=url)
    print(detail)
    return render(request, 'scan/bugdetail.html', {'url': url, 'detail': detail})